/**
 * Created by HP on 11/4/2016.
 */
(function () {
    angular
        .module("MyApp")
        .controller("TaskCtrl", TaskCtrl);

    TaskCtrl.$inject = ["$http"];

    function TaskCtrl($http) {
        var vm = this;

        vm.taskList = {
            status: "In-Progress", //please dont change
            task:"",
            dueDate: "",
            urgency:"Normal" // please dont change
        };

        vm.taskListArray = [];
        vm.todate = new Date();

        vm.editorEnabled = false;
        vm.editableTaskName = "";


        vm.result = null;

        vm.status = {
            message: "",
            success: ""
        };

        vm.deleted='false';


        vm.addToDo = addToDo;

        function addToDo() {



            $http.post("/api/tasks", vm.taskList)
                .then(function (response) {
                    console.info("Success! Response returned: " + JSON.stringify(response.data));
                    vm.taskListArray.push(vm.taskList);
                    vm.status.message = "The task is added to the database.";
                    vm.status.success = "ok";
                    console.info("status: " + JSON.stringify(vm.status) );
                })
                .catch(function (err) {
                    console.info("Error: " + err);
                    vm.status.message = "Failed to add the task to the database.";
                    vm.status.success = "error";
                });
        }

        vm.enableEditor = function () {
            vm.editorEnabled = true;
            vm.editableTaskName = vm.result.task_name;
        };

        vm.disableEditor = function () {
            vm.editorEnabled = false;
        };

        vm.save = function () {
            vm.result.task_name = vm.editableTaskName;
            //need to update to "/api/tasks/" ,{params:data}, currently put cannot identify which task to change
            $http.put("/api/tasks/",
                {
                    'task_name': vm.editableTaskName //this is the new task name alr. cannot use to find in db
                })
                .then(function (result) {
                    console.info("success");
                    vm.result = result.data;
                }).catch(function () {
                console.info("error");
                vm.status.message = "Failed to set the changes in task in the database.";
                vm.status.code = 400;
            });

            vm.disableEditor();
        };


    } // END TaskCtrl

})();