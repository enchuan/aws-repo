/**
 * Created by sswl on 4/11/16.
 */
(function(){
    angular
        .module("MyApp")
        .controller("UserCtrl", UserCtrl);

    UserCtrl.$inject = ["Upload"];

    function UserCtrl(Upload) {
        var vm = this;
        vm.imgFile = '/app/assets/images/defaultuser.jpeg';
        vm.status = {
            message: "",
            code: 0
        };
        vm.content = []; // variable that holds filenames returned from server

        vm.upload = function () {

            Upload.upload({
                url: '/upload',
                data:  {
                    "img-file": vm.imgFile
                }
            }).then(function (resp) {
                vm.fileurl = resp.data.filename;
                // console.log(vm.fileurl);
                // console.log(resp.data);
                vm.status.message = "Your profile picture is successfully updated";
                // vm.imgFile = "/app/assets/images/" + resp.data.filename;
                // console.log(resp.data.filename);
                // console.log(vm.imgFile);
                vm.status.code = 202;
            }).catch(function (err) {
                vm.status.message = "Fail to update the picture.";
                vm.status.code = 400
            });

        };
    }
})();
