/**
 * Created by Amulya on 19/10/2016.
 */
// This file is obsolete. The new file is in ./app/app.js
// This copy is kept to help transition
// Noted by Sam on 28 Oct.

(function() {

    var RegApp=angular.module("MyApp",[]);
    var MyCtrl = function () {
        var vm = this;

        vm.taskList = {
            status: "",
            task:"",
            dueDate: "",
            urgency:""
        };
        vm.taskListArray = [];
        vm.todate = new Date();

        // vm.taskListArray=angular.copy(vm.taskList);

        // myCtrl.row.forEach(function(v) {
        //
        // });

        vm.addToDo = function () {
            vm.taskListArray.push(vm.taskList);
            console.info(vm.taskListArray);
            vm.taskList ="";
        };

    };//close of controller
    RegApp.controller("MyCtrl",[MyCtrl]);


})();