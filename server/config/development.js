/**
 * Created by sswl on 28/10/16.
 */
module.exports = {
    mysql:{
        host: 'localhost',
        username: 'root',
        password: 'mysqlpw',
        database: 'todoList'
    },
    port: 3306
};

