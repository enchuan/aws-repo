/**
 * Created by sswl on 28/10/16.
 */
'use strict';

// NODE_ENV refers to production, the fallback is "development"
var ENV = process.env.NODE_ENV || "development";

module.exports = require ('./' + ENV +'.js');