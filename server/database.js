
var Sequelize = require ( "sequelize");
var config = require('./config');

//create a connection with DB

var sequelize = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password,
    {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });//end of sequelize


var UsersModal = require('./models/usersDB')(sequelize,Sequelize);
var TasksModal = require('./models/tasksDB')(sequelize,Sequelize);

UsersModal.hasMany(TasksModal);
TasksModal.belongsTo(UsersModal);

sequelize.sync({force: config.seed}) // to forcefully refresh DB
    .then(function(){
        console.log("Databse in Sync now");
    });

module.exports= {
    Users: UsersModal,
    Tasks: TasksModal
};