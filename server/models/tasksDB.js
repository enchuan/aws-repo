/**
 * Created by Amulya on 3/11/2016.
 */

module.exports = function (sequelize,Sequelize){
    return sequelize.define('tasks', {
        taskID: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        userID: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        importance: {
            type: Sequelize.ENUM('Normal','Important','Critical'),
            defaultValue: 'Normal'
        },
        status: {
            type: Sequelize.ENUM('Completed','In-Progress'),
            defaultValue: 'In-Progress'
        },
        dueDate: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
        }

    }, {
        tableName: 'tasks'
    });
};

