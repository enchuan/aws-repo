/**
 * Created by Amulya on 3/11/2016.
 */
/*
 userid
 email
 phone - need not always be present
 name
 password - atleast 8 bits in length

 */
module.exports = function(sequelize,Sequelize){

    return  sequelize.define('users', {
        userID: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey : true
        }
        ,
        email: {
            type: Sequelize.STRING,
            validate: { isEmail: true, isNull : false }
        },
        phone: {
            type: Sequelize.STRING
            , validate: {len : [8,11]}
        },
        firstName: {
            type: Sequelize.STRING,
            isNull: false
        },
        lastName: {
            type: Sequelize.STRING,
            isNull: false
        },
        password : {
            type: Sequelize.STRING,
            validate: { min: 8, isNull : false}
        }
    }, {
        tableName: 'users'
    });

};

