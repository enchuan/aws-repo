/**
 * Created by Amulya on 4/11/2016.
 */
var usersRoutes = require('./users.routes');
var tasksRoutes = require('./tasks.routes');

module.exports.set = function (app){


     usersRoutes.set(app);
     tasksRoutes.set(app);
};

