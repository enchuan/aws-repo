/**
 * Created by Amulya on 4/11/2016.
 */

var Tasks = require('./../database.js').Tasks;

module.exports.set = function (app){

    app.post('/api/tasks',function(req,res){
        Tasks.create(
            {
                userID: 1002, // setting default value - to move to client side
                description:req.body.task,
                importance: req.body.urgency,
                dueDate:req.body.dueDate,
                status:req.body.status
            }
        ).then(function(results){
            console.log("Inserted record: "+results);
            res.status(200).json(results);
        }).catch(function(error){
            console.log("Error trying to write to DB "+ error);
            res.status(500).send(JSON.stringify("Could not write to DB"));
        });

    }); //end of post

    // covers both complete and edit
    app.put('/api/tasks/', function (req, res) {
        Tasks.find({ where: { taskID: req.body.task.taskID } })
            .then(function (task) {
                // Check if record exists in db
                if (task) {
                    task.updateAttributes({
                        description: req.body.task.task_name,
                        status: req.body.task.status
                    }).then(function () {
                        console.log("Update successful.");
                        res.status(200).json(task);
                    }).catch(function(error){
                        console.log("Error trying to write to DB "+ error);
                        res.status(400).send(JSON.stringify("Failed to set the changes in task in the database."));
                    });
                }
            });
    });//end of put

    app.delete('/api/tasks', function (req, res) {
        console.log("Delete");
        console.log(req.body.taskID);

        Tasks.destroy({
            where: {
                taskID: req.body.taskID
            }
        }).then(function(result){
            console.log(result);
            res.status(200).json(result);
        }).catch(function(error){
            console.log("Error trying to delete task from DB "+ error);
            res.status(400).send(JSON.stringify("Failed to delete the task in the database."));
        });

    });


};
