
module.exports.set = function(app){

    var fs = require("fs");
    // fs stands for file system
    var path = require("path");
    // for dealing with relative and absolute paths
    var multer = require("multer");
    //var multipart = multer({dest: path.join(__dirname, "/upload_tmp/")});
    //__dirname + '/../../client/app/assets/imagesuploaded/',
    var storage = multer.diskStorage({
        destination: __dirname + '/../../client/app/assets/images/',
        filename: function (req, file, cb) {
            // cb stands for callback
            // console.log(file)
            // console.log("FROM CB "+__dirname);
            cb(null, "defaultuser.jpeg");
            // the position null occupies is for error
            // the error is up to programmer to define and "throw"
        }
    });

    var multipart = multer({ storage: storage });

    app.post("/upload", multipart.single("img-file"), function (req, res) {
        console.log(req.file);
        fs.readFile(req.file.path, function (err, data) {
            res.status(202).send({filename: req.file.filename, filesize: req.file.size});
            // res.send(202,req.file.filename);
        });
    });

};
