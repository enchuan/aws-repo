-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: todolist
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `taskID` int(11) NOT NULL,
  `userID` int(11) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `importance` enum('Normal','Important','Critical') DEFAULT 'Normal',
  `status` enum('Completed','In-Progress') DEFAULT 'In-Progress',
  `dueDate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`taskID`),
  KEY `userID` (`userID`),
  CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,1002,'Diane has to do laundry','Important','In-Progress','2016-11-04 14:17:12');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=1703 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1002,'Murphy','Diane','12345678','dmurphy@classicmodelcars.com','dummy'),(1056,'Patterson','Mary','12345678','mpatterso@classicmodelcars.com','dummy'),(1076,'Firrelli','Jeff','12345678','jfirrelli@classicmodelcars.com','dummy'),(1088,'Patterson','William','12345678','wpatterson@classicmodelcars.com','dummy'),(1102,'Bondur','Gerard','12345678','gbondur@classicmodelcars.com','dummy'),(1143,'Bow','Anthony','12345678','abow@classicmodelcars.com','dummy'),(1165,'Jennings','Leslie','12345678','ljennings@classicmodelcars.com','dummy'),(1166,'Thompson','Leslie','12345678','lthompson@classicmodelcars.com','dummy'),(1188,'Firrelli','Julie','12345678','jfirrelli@classicmodelcars.com','dummy'),(1216,'Patterson','Steve','12345678','spatterson@classicmodelcars.com','dummy'),(1286,'Tseng','Foon Yue','12345678','ftseng@classicmodelcars.com','dummy'),(1323,'Vanauf','George','12345678','gvanauf@classicmodelcars.com','dummy'),(1337,'Bondur','Loui','12345678','lbondur@classicmodelcars.com','dummy'),(1370,'Hernandez','Gerard','12345678','ghernande@classicmodelcars.com','dummy'),(1401,'Castillo','Pamela','12345678','pcastillo@classicmodelcars.com','dummy'),(1501,'Bott','Larry','12345678','lbott@classicmodelcars.com','dummy'),(1504,'Jones','Barry','12345678','bjones@classicmodelcars.com','dummy'),(1611,'Fixter','Andy','12345678','afixter@classicmodelcars.com','dummy'),(1612,'Marsh','Peter','12345678','pmarsh@classicmodelcars.com','dummy'),(1619,'King','Tom','12345678','tking@classicmodelcars.com','dummy'),(1621,'Nishi','Mami','12345678','mnishi@classicmodelcars.com','dummy'),(1625,'Kato','Yoshimi','12345678','ykato@classicmodelcars.com','dummy'),(1702,'Gerard','Martin','12345678','mgerard@classicmodelcars.com','dummy');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-04 14:50:41
